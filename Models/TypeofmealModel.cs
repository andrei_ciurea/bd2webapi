using System;
using System.Collections.Generic;


namespace bd2_project_fitness_assistant
{
    public class TypeofmealModel
    {
        public int ID {get;set;}
        public string TypeofmealName {get;set;}
      
     
        public TypeofmealModel(int ID, string TypeofmealNameRoleName){
            this.ID = ID;
            this.TypeofmealName = TypeofmealName;
        }   
    
        public TypeofmealModel(){
            
        }
    
    }
}