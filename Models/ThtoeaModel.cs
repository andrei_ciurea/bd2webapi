using System;
using System.Collections.Generic;


namespace bd2_project_fitness_assistant
{
    public  class ThtoeaModel 
    {
        public int ID {get;set;}

        public int Gm {get;set;}
        public int Kcal {get;set;}
        public int Kj {get;set;}
        public int TypeofmealID {get;set;}
        public string ThtoeaName {get;set;}
     
        public ThtoeaModel(int ID, int Gm, int Kcal,int Kj ,int TypeofmealID, string ThtoeaName){
            this.ID = ID;
            this.Gm = Gm;
            this.Kcal = Kcal;
            this.Kj = Kj;
            this.TypeofmealID = TypeofmealID;
            this.ThtoeaName = ThtoeaName;
        }   

        public ThtoeaModel(){
            
        }
    }
}