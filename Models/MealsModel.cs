using System;
using System.Collections.Generic;


namespace bd2_project_fitness_assistant
{
    public class MealsModel
    {
        public int ID {get;set;}

        public int UserID {get;set;}
        public int ThtoeaID {get;set;}
        public DateTime time {get;set;}
     
        public MealsModel(int ID, int UserID, int ThtoeaID, DateTime time){
            this.ID = ID;
            this.UserID = UserID;
            this.ThtoeaID = ThtoeaID;
            this.time = time;
        }   
        public MealsModel(){}
    }
}