using System;
using System.Collections.Generic;


namespace bd2_project_fitness_assistant
{
    public class UsersModel
    {
        public int ID {get;set;}
        public int RoleID {get;set;}
        public string Name {get;set;}
        public string Email {get;set;}
        public string Password {get;set;}
        public int Target{get;set;}
        public string Token {get;set;}
     
        public UsersModel(int ID, int RoleID, string Name, string Email, string Password, int Target){
            this.ID = ID;
            this.RoleID = RoleID;
            this.Name = Name;
            this.Email = Email;
            this.Password = Password;
            this.Target = Target;
        }   

        public UsersModel(){

        }

        public UsersModel(string name, string Password){
            this.Name = name;
            this.Password = Password;
        }
    }
}