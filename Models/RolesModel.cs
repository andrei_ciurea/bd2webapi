using System;
using System.Collections.Generic;


namespace bd2_project_fitness_assistant
{
    public class RolesModel
    {
        public int ID {get;set;}
        public string RoleName {get;set;}
      
     
        public RolesModel(int ID, string RoleName){
            this.ID = ID;
            this.RoleName = RoleName;
        }   
        public RolesModel(){}
    }
}