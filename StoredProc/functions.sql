CREATE OR REPLACE FUNCTION "Login"(name text, email text, password text)
      RETURNS INTEGER AS $$
DECLARE 
  uname TEXT DEFAULT '';
  uemail TEXT DEFAULT '';
  upassword TEXT DEFAULT '';
  exits INTEGER DEFAULT 0;
  recordu "Users"%ROWTYPE;
  cursor_users CURSOR(name TEXT) FOR SELECT * FROM "Users";  

BEGIN
	OPEN cursor_users(name);
	LOOP

		FETCH cursor_users INTO recordu;
		
		EXIT WHEN NOT FOUND;

		IF (recordu."Name" is not distinct from name) and (recordu."Password" is not distinct from password) THEN
			exits := 1;
		END IF;
		
	
	END LOOP;
  
	CLOSE cursor_users;
	
	RETURN exits;
        
END; $$ 
LANGUAGE 'plpgsql';

----------------------------------------------------------------------------------------------------



