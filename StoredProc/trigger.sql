CREATE OR REPLACE FUNCTION meals_trigger_function() RETURNS trigger AS $meals_trigger$
	BEGIN

		IF (TG_OP = 'DELETE') THEN
            
            
            INSERT INTO "triggerTypeOfMeal" ("time", "OldValue", "NewValue", "UserID", "Operation") VALUES('now'::timestamp,OLD,'', user,'D');
            RETURN OLD;
        ELSIF (TG_OP = 'UPDATE') THEN
            
            
            INSERT INTO "triggerTypeOfMeal" ("time", "OldValue", "NewValue", "UserID", "Operation") VALUES('now'::timestamp,OLD,NEW, user,'U');

            RETURN NEW;
        ELSIF (TG_OP = 'INSERT') THEN
            
            
	    INSERT INTO "triggerTypeOfMeal" ("time", "OldValue", "NewValue", "UserID", "Operation") VALUES('now'::timestamp,'o',NEW, user,'I');

            RETURN NEW;
        END IF;
		
	END;
$meals_trigger$ LANGUAGE plpgsql;


CREATE TRIGGER meals_trigger
AFTER INSERT OR UPDATE OR DELETE ON "Thtoea"
    FOR EACH ROW EXECUTE PROCEDURE meals_trigger_function();
