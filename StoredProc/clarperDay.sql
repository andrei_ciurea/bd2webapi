
CREATE OR REPLACE FUNCTION "CalPerDay"(userID integer,num_of_days integer)
      RETURNS NUMERIC[] AS $$
DECLARE 
  integr_array numeric[];
	targetu numeric;
	sumetotargetu numeric;
	iterator integer := 0;
  

BEGIN

	select "Target" into targetu from "Users" where ID = userID;

	integr_array = array_append(integr_array,targetu);	
	
	WHILE iterator < num_of_days
	LOOP	
		select sum("Kcal") into sumetotargetu from "Meals" where time = 'now'::timestamp - (iterator::text || ' days')::interval and "UserID" = userID;

		integr_array = array_append(integr_array,sumetotargetu);
		iterator := iterator + 1;		

	END LOOP;	
	
	
	RETURN integr_array;
        
END; $$ 
LANGUAGE 'plpgsql';

----------------------------------------------------------------------------------------------------

