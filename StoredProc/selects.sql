CREATE OR REPLACE FUNCTION "usersSelectU"(username text)
      RETURNS SETOF "Users" AS $$
BEGIN
	IF userName is not distinct from '' THEN
		RETURN QUERY SELECT * FROM "Users";
	ELSE
		RETURN QUERY SELECT * FROM "Users" where "Name" = username;
	END IF;
END; $$ 
LANGUAGE 'plpgsql';

------------------------------------------------------------

CREATE OR REPLACE FUNCTION "usersSelectE"(uemail text)
      RETURNS SETOF "Users" AS $$
BEGIN
	IF uemail is not distinct from '' THEN
		RETURN QUERY SELECT * FROM "Users";
	ELSE
		RETURN QUERY SELECT * FROM "Users" where "email" = uemail;
	END IF;
END; $$ 
LANGUAGE 'plpgsql';

------------------------------------------------------------

CREATE OR REPLACE FUNCTION "usersSelectI"(id INTEGER)
      RETURNS SETOF "Users" AS $$
BEGIN
	IF id = -1 THEN
		RETURN QUERY SELECT * FROM "Users";
	ELSE
		RETURN QUERY SELECT * FROM "Users" where "ID" = id;
	END IF;
END; $$ 
LANGUAGE 'plpgsql';

------------------------------------------------------------


CREATE OR REPLACE FUNCTION "rolesSelectU"(roleName text)
      RETURNS SETOF "Roles" AS $$
BEGIN
	IF roleName is not distinct from '' THEN
		RETURN QUERY SELECT * FROM "Roles";
	ELSE
		RETURN QUERY SELECT * FROM "Roles" where "RoleName" = roleName;
	END IF;
END; $$ 
LANGUAGE 'plpgsql';

------------------------------------------------------------

CREATE OR REPLACE FUNCTION "rolesSelectI"(id INTEGER)
      RETURNS SETOF "Roles" AS $$
BEGIN
	IF id = -1 THEN
		RETURN QUERY SELECT * FROM "Roles";
	ELSE
		RETURN QUERY SELECT * FROM "Roles" where "ID" = id;
	END IF;
END; $$ 
LANGUAGE 'plpgsql';

------------------------------------------------------------

CREATE OR REPLACE FUNCTION "mealsSelectI"(id INTEGER)
      RETURNS SETOF "Meals" AS $$
BEGIN
	IF id == -1 THEN
		RETURN QUERY SELECT * FROM "Meals";
	ELSE
		RETURN QUERY SELECT * FROM "Meals" where "ID" = id;
	END IF;
END; $$ 
LANGUAGE 'plpgsql';

------------------------------------------------------------


CREATE OR REPLACE FUNCTION "mealsSelectIU"(userID INTEGER)
      RETURNS SETOF "Meals" AS $$
BEGIN
	IF userID = -1 THEN
		RETURN QUERY SELECT * FROM "Meals";
	ELSE
		RETURN QUERY SELECT * FROM "Meals" where "UserID" = userID;
	END IF;
END; $$ 
LANGUAGE 'plpgsql';

------------------------------------------------------------


CREATE OR REPLACE FUNCTION "mealsSelectIUT"(num_months INTEGER,userID INTEGER)
      RETURNS SETOF "Meals" AS $$
BEGIN
	
		RETURN QUERY SELECT * FROM "Meals" where "time" > 'now'::timestamp - 
(num_months::text || ' month')::interval and "UserID" = userID;
END; $$ 
LANGUAGE 'plpgsql';

------------------------------------------------------------


CREATE OR REPLACE FUNCTION "thtoeaSelectI"(id INTEGER)
      RETURNS SETOF "Thtoea" AS $$
BEGIN
	IF id = -1 THEN
		RETURN QUERY SELECT * FROM "Thtoea";
	ELSE
		RETURN QUERY SELECT * FROM "Thtoea" where "ID" = id;
	END IF;
END; $$ 
LANGUAGE 'plpgsql';

------------------------------------------------------------

CREATE OR REPLACE FUNCTION "typeofmealSelectI"(id INTEGER)
      RETURNS SETOF "Typeofmeal" AS $$
BEGIN
	IF id = -1 THEN
		RETURN QUERY SELECT * FROM "Typeofmeal";
	ELSE
		RETURN QUERY SELECT * FROM "Typeofmeal" where "ID" = id;
	END IF;
END; $$ 
LANGUAGE 'plpgsql';

------------------------------------------------------------

CREATE OR REPLACE FUNCTION "selectlastIDFromMeals"()
      RETURNS numeric AS $$
DECLARE
	id numeric;
BEGIN
	select "ID" into id from "Thtoea" order by "ID" DESC LIMIT 1;
	id := id + 1;
	RETURN id;
END; $$ 
LANGUAGE 'plpgsql';

------------------------------------------------------------

