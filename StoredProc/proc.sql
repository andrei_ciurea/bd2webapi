
CREATE OR REPLACE FUNCTION "fastfoodPercUser"(userID integer,num_of_days integer, typeofmealID integer)
      RETURNS NUMERIC AS $$
DECLARE 
  procentaj numeric;
	sumAll numeric;
	sumaType numeric;
  

BEGIN

	IF userID != 0 THEN
		select count(*) into sumall from "Meals" where "UserID" = userID and "time" = 'now'::timestamp - (iterator::text || ' days')::interval;
		select count(*) into sumaType from "Meals" where "UserID" = userID and "TypeofmealID" = typeofmealID and "time" = 'now'::timestamp - (iterator::text || ' days')::interval;
		
		procentaj := sumaType / sumaAll;
	ELSE
	
		select count(*) into sumall from "Meals" where "time" = 'now'::timestamp - (iterator::text || ' days')::interval;
		select count(*) into sumaType from "Meals" where "TypeofmealID" = typeofmealID and "time" = 'now'::timestamp - (iterator::text || ' days')::interval;

		procentaj := sumaType / sumaAll;

	END IF;

	RETURN procentaj;
        
END; $$ 
LANGUAGE 'plpgsql';

----------------------------------------------------------------------------------------------------

