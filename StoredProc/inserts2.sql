CREATE OR REPLACE FUNCTION "insertUser"(uname text, uemail text, upassword text, utarget numeric) 
RETURNS void AS $$

BEGIN

	insert into "Users" ("Name", "Email", "Password", "Target") values (uname,uemail,upassword,utarget);
        
END; $$ 
LANGUAGE 'plpgsql';

----------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION "insertThtoea"(ugm numeric,ukcal numeric,ukj numeric,typeofmeal integer,uname text) RETURNS void AS $$

BEGIN

	insert into "Thtoea" ("Gm", "Kcal", "Kj", "TypeofmealID","ThtoeaName") values (ugm,ukcal,ukj,typeofmeal,uname);
        
END; $$ 
LANGUAGE 'plpgsql';

----------------------------------------------------------------------------------------

CREATE OR REPLACE FUNCTION "insertMeals"(uuserid integer,uthtoeat integer,utime timestamp) RETURNS void AS $$

BEGIN

	insert into "Meals" ("UserID", "ThtoeaID","time") values (uuserid, uthtoeat,utime);
        
END; $$ 
LANGUAGE 'plpgsql';

----------------------------------------------------------------------------------------
