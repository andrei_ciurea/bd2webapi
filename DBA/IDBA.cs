
using System;
using System.Collections.Generic;
using System.Data;
using Npgsql;

namespace bd2_project_fitness_assistant{

    public interface IDBA{
        UsersModel selectUsers(string userName);
        UsersModel selectUsersE(string email);
        UsersModel selectUsersI(int id);
        List<UsersModel> selectUsersList();
        RolesModel rolesSelectU(string roleName);
        RolesModel rolesSelectI(int id);
        List<RolesModel> rolesSelectALL();
        MealsModel mealsSelectI(int id);
        MealsModel mealsSelectIU(int userID);
        MealsModel mealsSelectIUT(int num_months,int userID);
        List<MealsModel> mealsSelectALL();
        ThtoeaModel thtoeaSelectI(int id);
        List<ThtoeaModel> thtoeaSelectALL();
        TypeofmealModel typeofmealSelectI(int id);
        List<TypeofmealModel> typeofmealSelectAll();
        void insertUser(string uname, string uemail , string upassword , int utarget );
        void insertThtoea(int ugm,int ukcal,int ukj,int typeofmeal, string uname  );
        void insertMeals(int uuserID,int uthtoeat,DateTime utime );
        List<int> calperDay(int userID,int num_of_days);
        int fastfoodPercUser(int userID,int num_of_days,int typeofmealID);
        int Login(string name,string email, string password);
        UsersModel Authenticate(string username, string password);
        int selectlastIDFromMeals();
        void Dispose();
    } 
}