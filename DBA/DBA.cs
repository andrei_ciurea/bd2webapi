using System;
using System.Collections.Generic;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using Npgsql;

namespace bd2_project_fitness_assistant
{
    public class DBA : IDisposable, IDBA
    {

        NpgsqlConnection connection;
        string connectionString = "Server=127.0.0.1;Port=5432;Database=bd2;User Id=andrei; Password=andrei;";

        public DBA(){
            connection = new NpgsqlConnection(connectionString);
            if(connection != null && connection.State != ConnectionState.Open)
                connection.Open();
            // if(connection.State == ConnectionState.Open)
            //     Console.WriteLine("connectat");
            // else
            //     Console.WriteLine("!connectat");
            // Console.ReadLine();
            
        }
//SELECTS

//GET USERS
        public UsersModel selectUsers(string userName){
            UsersModel user = new UsersModel();
            
            NpgsqlCommand pgcomm = new NpgsqlCommand("\"usersSelectU\"",connection);
            pgcomm.CommandType = CommandType.StoredProcedure;
            pgcomm.Parameters.AddWithValue(":username",userName);
            NpgsqlDataReader pgreader = pgcomm.ExecuteReader();

            while(pgreader.Read()){
                user.ID = pgreader.GetInt32(0);
                user.RoleID = pgreader.IsDBNull(1) ? 0 : pgreader.GetInt32(1);
                user.Name = pgreader.GetString(2);
                user.Email = pgreader.GetString(3);
                user.Password = pgreader.GetString(4);
                user.Target = pgreader.GetInt32(5);
                
            }
            pgreader.Close();
            return user;
        }

        public UsersModel selectUsersE(string email){
            UsersModel user = new UsersModel();
            
            NpgsqlCommand pgcomm = new NpgsqlCommand("\"usersSelectU\"",connection);
            pgcomm.CommandType = CommandType.StoredProcedure;
            pgcomm.Parameters.AddWithValue(":uemail",email);
            NpgsqlDataReader pgreader = pgcomm.ExecuteReader();

            while(pgreader.Read()){
                user.ID = pgreader.GetInt32(0);
                user.RoleID = pgreader.IsDBNull(1) ? 0 : pgreader.GetInt32(1);
                user.Name = pgreader.GetString(2);
                user.Email = pgreader.GetString(3);
                user.Password = pgreader.GetString(4);
                user.Target = pgreader.GetInt32(5);
                
            }
            pgreader.Close();
            return user;
        }

        public UsersModel selectUsersI(int id){
            UsersModel user = new UsersModel();
            
            NpgsqlCommand pgcomm = new NpgsqlCommand("\"usersSelectU\"",connection);
            pgcomm.CommandType = CommandType.StoredProcedure;
            pgcomm.Parameters.AddWithValue(":id",id);
            NpgsqlDataReader pgreader = pgcomm.ExecuteReader();

            while(pgreader.Read()){
                user.ID = pgreader.GetInt32(0);
                user.RoleID = pgreader.IsDBNull(1) ? 0 : pgreader.GetInt32(1);
                user.Name = pgreader.GetString(2);
                user.Email = pgreader.GetString(3);
                user.Password = pgreader.GetString(4);
                user.Target = pgreader.GetInt32(5);
                
            }
            pgreader.Close();
            return user;
        }

        public List<UsersModel> selectUsersList(){
            List<UsersModel> listUser = new List<UsersModel>();
            
            
            NpgsqlCommand pgcomm = new NpgsqlCommand("\"usersSelectU\"",connection);
            pgcomm.CommandType = CommandType.StoredProcedure;
            pgcomm.Parameters.AddWithValue("username","");
            
            NpgsqlDataReader pgreader = pgcomm.ExecuteReader();

            while(pgreader.Read()){
                UsersModel user = new UsersModel();
                
                user.ID = pgreader.GetInt32(0);
                user.RoleID = pgreader.IsDBNull(1) ? 0 : pgreader.GetInt32(1);
                user.Name = pgreader.GetString(2);
                user.Email = pgreader.GetString(3);
                user.Password = pgreader.GetString(4);
                user.Target = pgreader.IsDBNull(5) ? 0 : pgreader.GetInt32(5);
                
                listUser.Add(user);
            }
            pgreader.Close();
            return listUser;
        }
    //GET Roles

        public RolesModel rolesSelectU(string roleName){
            RolesModel role = new RolesModel();
            
            NpgsqlCommand pgcomm = new NpgsqlCommand("\"rolesSelectU\"",connection);
            pgcomm.CommandType = CommandType.StoredProcedure;
            pgcomm.Parameters.AddWithValue(":roleName",roleName);
            NpgsqlDataReader pgreader = pgcomm.ExecuteReader();

            while(pgreader.Read()){
                role.ID = pgreader.GetInt32(0);
                role.RoleName = pgreader.GetString(1);
                
                
            }
            pgreader.Close();
            return role;
        }

        public RolesModel rolesSelectI(int id){
            RolesModel role = new RolesModel();
            
            NpgsqlCommand pgcomm = new NpgsqlCommand("\"rolesSelectI\"",connection);
            pgcomm.CommandType = CommandType.StoredProcedure;
            pgcomm.Parameters.AddWithValue(":id",id);
            NpgsqlDataReader pgreader = pgcomm.ExecuteReader();

            while(pgreader.Read()){
                role.ID = pgreader.GetInt32(0);
                role.RoleName = pgreader.GetString(1);
                
                
            }
            pgreader.Close();
            return role;
        }

        public List<RolesModel> rolesSelectALL(){
            List<RolesModel> listRole = new List<RolesModel>();
            
            NpgsqlCommand pgcomm = new NpgsqlCommand("\"rolesSelectI\"",connection);
            pgcomm.CommandType = CommandType.StoredProcedure;
            pgcomm.Parameters.AddWithValue(":id",-1);
            NpgsqlDataReader pgreader = pgcomm.ExecuteReader();

            while(pgreader.Read()){
                RolesModel role = new RolesModel();

                role.ID = pgreader.GetInt32(0);
                role.RoleName = pgreader.GetString(1);
                
                listRole.Add(role);
            }
            pgreader.Close();
            return listRole;
        }

    //GET Meals
        public MealsModel mealsSelectI(int id){
            MealsModel meals = new MealsModel();
            
            NpgsqlCommand pgcomm = new NpgsqlCommand("\"mealsSelectI\"",connection);
            pgcomm.CommandType = CommandType.StoredProcedure;
            pgcomm.Parameters.AddWithValue(":id",id);
            NpgsqlDataReader pgreader = pgcomm.ExecuteReader();

            while(pgreader.Read()){
                meals.ID = pgreader.GetInt32(0);
                meals.UserID = pgreader.GetInt32(1);
                meals.ThtoeaID = pgreader.GetInt32(2);
                meals.time = pgreader.GetDateTime(3);
                
            }
            pgreader.Close();
            return meals;
        }

        public MealsModel mealsSelectIU(int userID){
            MealsModel meals = new MealsModel();
            
            NpgsqlCommand pgcomm = new NpgsqlCommand("\"mealsSelectU\"",connection);
            pgcomm.CommandType = CommandType.StoredProcedure;
            pgcomm.Parameters.AddWithValue(":userID",userID);
            NpgsqlDataReader pgreader = pgcomm.ExecuteReader();

            while(pgreader.Read()){
                meals.ID = pgreader.GetInt32(0);
                meals.UserID = pgreader.GetInt32(1);
                meals.ThtoeaID = pgreader.GetInt32(2);
                meals.time = pgreader.GetDateTime(3);
                
            }
            pgreader.Close();
            return meals;
        }

        public MealsModel mealsSelectIUT(int num_months,int userID){
            MealsModel meals = new MealsModel();
            
            NpgsqlCommand pgcomm = new NpgsqlCommand("\"mealsSelectIUT\"",connection);
            pgcomm.CommandType = CommandType.StoredProcedure;
            pgcomm.Parameters.AddWithValue(":num_months",num_months);
            pgcomm.Parameters.AddWithValue(":userID",userID);
            NpgsqlDataReader pgreader = pgcomm.ExecuteReader();

            while(pgreader.Read()){
                meals.ID = pgreader.GetInt32(0);
                meals.UserID = pgreader.GetInt32(1);
                meals.ThtoeaID = pgreader.GetInt32(2);
                meals.time = pgreader.GetDateTime(3);
                
            }
            pgreader.Close();
            return meals;
        }

        public List<MealsModel> mealsSelectALL(){
            List<MealsModel> listMeals = new List<MealsModel>();
            
            NpgsqlCommand pgcomm = new NpgsqlCommand("\"mealsSelectI\"",connection);
            pgcomm.CommandType = CommandType.StoredProcedure;
            pgcomm.Parameters.AddWithValue(":id",-1);
            NpgsqlDataReader pgreader = pgcomm.ExecuteReader();

            while(pgreader.Read()){
                MealsModel meals = new MealsModel();

                meals.ID = pgreader.GetInt32(0);
                meals.UserID = pgreader.GetInt32(1);
                meals.ThtoeaID = pgreader.GetInt32(2);
                meals.time = pgreader.GetDateTime(3);
                
                listMeals.Add(meals);
            }
            pgreader.Close();
            return listMeals;
        }

        public int selectlastIDFromMeals(){
            int id = 0;
            
            NpgsqlCommand pgcomm = new NpgsqlCommand("\"selectlastIDFromMeals\"",connection);
            pgcomm.CommandType = CommandType.StoredProcedure;
            NpgsqlDataReader pgreader = pgcomm.ExecuteReader();

            while(pgreader.Read()){
              id = pgreader.GetInt32(0);
            }
            pgreader.Close();
            return id;
        }


    //GET Thtoea
        public ThtoeaModel thtoeaSelectI(int id){
            ThtoeaModel thtoea = new ThtoeaModel();
            
            NpgsqlCommand pgcomm = new NpgsqlCommand("\"thtoeaSelectI\"",connection);
            pgcomm.CommandType = CommandType.StoredProcedure;
            pgcomm.Parameters.AddWithValue(":id",id);
            NpgsqlDataReader pgreader = pgcomm.ExecuteReader();

            while(pgreader.Read()){
                thtoea.ID = pgreader.GetInt32(0);
                thtoea.Gm = pgreader.GetInt32(1);
                thtoea.Kcal = pgreader.GetInt32(2);
                thtoea.Kj = pgreader.GetInt32(3);
                thtoea.TypeofmealID = pgreader.GetInt32(4);
                thtoea.ThtoeaName = pgreader.GetString(5);
                
            }
            pgreader.Close();
            return thtoea;
        }

        public List<ThtoeaModel> thtoeaSelectALL(){
            List<ThtoeaModel> listTh = new List<ThtoeaModel>();
            
            NpgsqlCommand pgcomm = new NpgsqlCommand("\"thtoeaSelectI\"",connection);
            pgcomm.CommandType = CommandType.StoredProcedure;
            pgcomm.Parameters.AddWithValue(":id",-1);
            NpgsqlDataReader pgreader = pgcomm.ExecuteReader();

            while(pgreader.Read()){
                ThtoeaModel thtoea = new ThtoeaModel();

                thtoea.ID = pgreader.GetInt32(0);
                thtoea.Gm = pgreader.GetInt32(1);
                thtoea.Kcal = pgreader.GetInt32(2);
                thtoea.Kj = pgreader.GetInt32(3);
                thtoea.TypeofmealID = pgreader.GetInt32(4);
                thtoea.ThtoeaName = pgreader.GetString(5);

                listTh.Add(thtoea);
                
            }
            pgreader.Close();
            return listTh;
        }
    //GET Typeofmeal
        public TypeofmealModel typeofmealSelectI(int id){
            TypeofmealModel typeofmeal = new TypeofmealModel();
            
            NpgsqlCommand pgcomm = new NpgsqlCommand("\"typeofmealSelectI\"",connection);
            pgcomm.CommandType = CommandType.StoredProcedure;
            pgcomm.Parameters.AddWithValue(":id",id);
            NpgsqlDataReader pgreader = pgcomm.ExecuteReader();

            while(pgreader.Read()){
                typeofmeal.ID = pgreader.GetInt32(0);
                typeofmeal.TypeofmealName = pgreader.GetString(1);
                
            }
            pgreader.Close();
            return typeofmeal;
        }

        public List<TypeofmealModel> typeofmealSelectAll(){
            List<TypeofmealModel> listType = new List<TypeofmealModel>();
            
            NpgsqlCommand pgcomm = new NpgsqlCommand("\"typeofmealSelectI\"",connection);
            pgcomm.CommandType = CommandType.StoredProcedure;
            pgcomm.Parameters.AddWithValue(":id",-1);
            NpgsqlDataReader pgreader = pgcomm.ExecuteReader();

            while(pgreader.Read()){
                TypeofmealModel typeofmeal = new TypeofmealModel();

                typeofmeal.ID = pgreader.GetInt32(0);
                typeofmeal.TypeofmealName = pgreader.GetString(1);
                
                listType.Add(typeofmeal);
            }
            pgreader.Close();
            return listType;
        }
//INSERTS
        public void insertUser(string uname, string uemail , string upassword , int utarget ){
            //insertUser
            NpgsqlCommand pgcomm = new NpgsqlCommand("\"insertUser\"", connection);
            pgcomm.CommandType = CommandType.StoredProcedure;
            pgcomm.Parameters.AddWithValue(":uname", uname);
            pgcomm.Parameters.AddWithValue(":uemail", uemail);
            pgcomm.Parameters.AddWithValue(":upassword", upassword);
            pgcomm.Parameters.AddWithValue(":utarget", utarget);

            pgcomm.ExecuteNonQuery();
        }

        public void insertThtoea(int ugm,int ukcal,int ukj,int typeofmeal, string uname  ){
            //ugm numeric,ukcal numeric,ukj numeric,typeofmeal integer,uname text
            NpgsqlCommand pgcomm = new NpgsqlCommand("\"insertThtoea\"", connection);
            pgcomm.CommandType = CommandType.StoredProcedure;
        
            pgcomm.Parameters.AddWithValue(":ugm", ugm);
            pgcomm.Parameters.AddWithValue(":ukcal", ukcal);
            pgcomm.Parameters.AddWithValue(":ukj", ukj);
            pgcomm.Parameters.AddWithValue(":typeofmeal", typeofmeal);
            pgcomm.Parameters.AddWithValue(":uname", uname);

            pgcomm.ExecuteNonQuery();
        }

        public void insertMeals(int uuserID,int uthtoeat,DateTime utime ){
            //uuserID integer,uthtoeat integer,utime timestamp
            NpgsqlCommand pgcomm = new NpgsqlCommand("\"insertMeals\"", connection);
            pgcomm.CommandType = CommandType.StoredProcedure;
        
            pgcomm.Parameters.AddWithValue(":uuserid", uuserID);
            pgcomm.Parameters.AddWithValue(":uthtoeat", uthtoeat);
            pgcomm.Parameters.AddWithValue(":utime", utime);

            pgcomm.ExecuteNonQuery();
        }

        public List<int> calperDay(int userID,int num_of_days){
            List<int> calsPerDay = new List<int>();

            NpgsqlCommand pgcomm = new NpgsqlCommand("\"CalPerDay\"",connection);
            pgcomm.CommandType = CommandType.StoredProcedure;

            pgcomm.Parameters.AddWithValue(":userID",userID);
            pgcomm.Parameters.AddWithValue(":num_of_days",num_of_days);

            NpgsqlDataReader pgreader = pgcomm.ExecuteReader();

            while(pgreader.Read()){
                calsPerDay.Add(pgreader.GetInt32(0));
            }
            pgreader.Close();
            return calsPerDay;
        }

        public int fastfoodPercUser(int userID,int num_of_days,int typeofmealID){
            int proc = 0;

            NpgsqlCommand pgcomm = new NpgsqlCommand("\"fastfoodPercUser\"",connection);
            pgcomm.CommandType = CommandType.StoredProcedure;

            pgcomm.Parameters.AddWithValue(":userID",userID);
            pgcomm.Parameters.AddWithValue(":num_of_days",num_of_days);
            pgcomm.Parameters.AddWithValue(":typeofmealID",typeofmealID);

            NpgsqlDataReader pgreader = pgcomm.ExecuteReader();

            while(pgreader.Read()){
                proc = pgreader.GetInt32(0);
            }
            pgreader.Close();
            return proc;
        }

        public int Login(string name,string email, string password){
            int exists = 0;
            
            NpgsqlCommand pgcomm = new NpgsqlCommand("\"Login\"",connection);
            pgcomm.CommandType = CommandType.StoredProcedure;

            pgcomm.Parameters.AddWithValue(":name",name);
            pgcomm.Parameters.AddWithValue(":email",email);
            pgcomm.Parameters.AddWithValue(":password",password);

            NpgsqlDataReader pgreader = pgcomm.ExecuteReader();

            while(pgreader.Read()){
                exists = pgreader.GetInt32(0);
            }
            pgreader.Close();

            return exists;
        }

        public UsersModel Authenticate(string username, string password)
        {
            
            UsersModel user = selectUsers(username);

            // return null if user not found
            if (user == null)
                return null;

            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("secretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecretsecret");
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[] 
                {
                    new Claim(ClaimTypes.Name, user.ID.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            // remove password before returning
            user.Password = "";

            return user;
        }
        public void Dispose()
        {
            if(connection != null && connection.State == ConnectionState.Open)
                connection.Close();
        }
    }
}
