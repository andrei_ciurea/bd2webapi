using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using bd2_project_fitness_assistant;
using Microsoft.AspNetCore.Mvc;

namespace bd2_fitness_assistant.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class Values_secController : ControllerBase
    {
        private readonly IDBA _dba; 

        public Values_secController(IDBA dba){
            this._dba = dba;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            List<UsersModel> list = _dba.selectUsersList();
            return list.Select(x => x.Name).ToArray();
        }

        // GET api/values/5s
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}