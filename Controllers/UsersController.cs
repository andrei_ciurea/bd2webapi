using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using bd2_project_fitness_assistant;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace bd2_fitness_assistant.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {

        private readonly IDBA _dba; 

        public UsersController(IDBA dba){
            this._dba = dba;
        }

        // GET api/users
        [HttpGet]
        public ActionResult<IEnumerable<UsersModel>> Get()
        {
            List<UsersModel> list = _dba.selectUsersList();
            return list;
        }

        // GET api/users/5
        [HttpGet("{id}")]
        public ActionResult<UsersModel> Get(int id)
        {
            return _dba.selectUsersI(id);
        }


        // DELETE api/users/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        //authenticate
        [AllowAnonymous]
        [HttpPost]
        [Route("[action]")]
        public IActionResult authenticate([FromBody] UsersModel user)
        {
            
                Console.WriteLine(user.Name + " no ");
                int ok_login = _dba.Login(user.Name,"",user.Password);
                UsersModel usersModel = _dba.Authenticate(user.Name, user.Password);
                if(ok_login == 1){
                    if(usersModel == null){
                        return BadRequest(new { message = "Username or password is incorrect" });
                    }else{
                        return Ok(usersModel);
                    }
                }else{
                    return BadRequest(new { message = "Username or password is incorrect" });
                }
            

        }

        // POST api/Users/register
        [HttpPost]
        [Route("[action]")]
        public void register([FromBody] UsersModel user)
        {
            _dba.insertUser(user.Name,user.Email, user.Password, user.Target);
        }
        
    }
}
