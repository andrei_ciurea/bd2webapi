using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using bd2_project_fitness_assistant;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace bd2_fitness_assistant.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MealsController : ControllerBase
    {

        private readonly IDBA _dba; 

        public MealsController(IDBA dba){
            this._dba = dba;
        }

        // GET api/users
        [HttpGet]
        public ActionResult<IEnumerable<TypeofmealModel>> Get()
        {
            List<TypeofmealModel> list = _dba.typeofmealSelectAll();
            return list;
        }

        // GET api/users/5
        [HttpGet("{id}")]
        public ActionResult<MealsModel> Get(int id)
        {
            return _dba.mealsSelectI(id);
        }

        [HttpGet]
        [Route("[action]")]
        public ActionResult<int> GetLastIDMeals()
        {   int id = _dba.selectlastIDFromMeals();
            Console.WriteLine(id);

            return id;
        }

        // DELETE api/users/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }

        //authenticate
        [AllowAnonymous]
        [HttpPost]
        [Route("[action]")]
        public void meal([FromBody] MealsModel meal)
        {
            _dba.insertMeals(meal.UserID, meal.ThtoeaID,meal.time);          
        }

        // POST api/Users/register
        [HttpPost]
        [Route("[action]")]
        public ThtoeaModel thtoea([FromBody] ThtoeaModel thtoea)
        {
            //Console.ReadLine();
            //Console.WriteLine(thtoea.ThtoeaName + "----------------------------------");
            _dba.insertThtoea(thtoea.Gm,thtoea.Kcal,thtoea.Kj,thtoea.TypeofmealID, thtoea.ThtoeaName);
            //Console.WriteLine(_dba.thtoeaSelectALL().Where(x => x.ThtoeaName.Equals(thtoea.ThtoeaName)).FirstOrDefault() + "----------------------------");
            
            return _dba.thtoeaSelectALL().Where(x => x.ThtoeaName.Equals(thtoea.ThtoeaName)).FirstOrDefault();
        }

        //thget

        [HttpGet]
        [Route("[action]")]
        public ActionResult<ThtoeaModel> thget(string name)
        {
            return _dba.thtoeaSelectALL().Where(x => x.ThtoeaName.Equals(name)).FirstOrDefault();
        }
        //thgetAll
        [HttpGet]
        [Route("[action]")]
        public ActionResult<IEnumerable< ThtoeaModel>> thgetAll()
        {
            return _dba.thtoeaSelectALL();
        }

        //mealIn
         [HttpPost]
        [Route("[action]")]
        public void mealIn([FromBody] MealsModel meals)
        {
            //Console.ReadLine();
            
            Console.WriteLine(meals.ThtoeaID);
            Console.ReadLine();
            _dba.insertMeals(meals.UserID, meals.ThtoeaID, meals.time );
            
        }
    }
}
